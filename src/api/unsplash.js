import axios from "axios";

export default axios.create({
  baseURL: "https://api.unsplash.com",
  headers: {
    Authorization:
      "Client-ID d9ffeef33a266633452c4ccd4a3dde640fbd3a1b3cfc0057c8b362e8bd76954e"
  }
});
